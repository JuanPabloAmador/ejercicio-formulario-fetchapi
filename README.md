# Ejercicio Formulario fetchapi

MUESTRA DE ENVIO DE DATOS Y VALIDACIÓN DE FORMULARIO CON FETCH, JAVASCRIPT PHP Y JSON
=====================================================================================

En primer lugar y para darle a su autor el correspondiente credito, éste repositorio contiene un ejercicio resuelto encontrado en [https://youtu.be/nLrL9Ip3tWI](https://youtu.be/nLrL9Ip3tWI) 
video que pertenece al canal Bluuweb! en Youtube.

CONTENIDO DEL REPOSITORIO
=========================

El repo contiene 5 archivos además del readme,
*  .gitlab-ci.yml
*  docker-compose.yml
*  index.html
*  post.php
*  app.js

El archivo **.gitlab-ci.yml** es el archivo de configuración para levantar un guitlab pages, con la vista del html estático del proyecto, este proyecto al tener contenido dinamico que depende
de un archivo PHP en gitlab pages solo se mostrará la parte estática del HTML y no funcionará como tal la aplicación sin embargo dejo igualmente la URL al gitlab pages, de cara a que se vea
como se vé el archivo HTML: 
Enlace al gitlab pages de la página: [https://juanpabloamador.gitlab.io/ejercicio-formulario-fetchapi/](https://juanpabloamador.gitlab.io/ejercicio-formulario-fetchapi/)

El archivo **docker-compose.yml** contiene instrucciones para levantar un contenedor de Docker que contenga un servidor Apache con PHP, que además mapeará como
volumen permanente la ruta en la que se encuentre la terminal a la hora de hacer el comando **docker-compose up -d** además el servidor apache se encuentra
escuchando en el puerto **8080**.

En cuanto al **index.html** lo unico a resaltar es que el formulario está construido con bootstrap, de ahí los scripts que se encuentran al final de la 
etiqueta body. 
