//console.log("Funcionando"); para saber que el archivo está enlazado

var formulario = document.getElementById("formulario");
var respuesta = document.getElementById("respuesta");


formulario.addEventListener("submit", function (e) {

    e.preventDefault();
    //console.log("Escuchando") para saber que estamos escuchando el boton submit


    var datos = new FormData(formulario);

    //console.log(datos); saca el FormData pero sin sus valores accesibles
    //console.log(datos.get("usuario")) //Saca el value, que corresponda con el name del campo de entradas
    //console.log(datos.get("pass"))

    fetch('post.php', {
        method: 'POST',
        body: datos
    })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data ==='error'){
                respuesta.innerHTML =
                `
                <div class="alert alert-danger" role="alert">
                    Llena todos los campos
                </div>`
            } else {
                respuesta.innerHTML =
                `
                <div class="alert alert-primary" role="alert">
                    ${data}
                </div>`
            }
        })


})